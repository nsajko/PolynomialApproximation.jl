# Copyright © 2023 Neven Sajko. All rights reserved.

module IntervalHelpers

using PolynomialPassingThroughIntervals.NumericalErrorTypes

export
  intervals_given_max_abs_err, intervals_given_max_rel_err,
  intervals_given_max_err,
  intervals_for_fixed_point_lookup_table_emulation

interval_given_max_abs_err(val::AbstractFloat, max_err::Real) =
  (val - max_err, val + max_err)

interval_given_max_abs_err(max_err::Real) =
  let e = max_err
    v -> interval_given_max_abs_err(v, e)
  end

function intervals_given_max_abs_err(
  vals::AbstractVector{<:AbstractFloat},
  max_err::Real)

  (0 < max_err) || error("max error not positive")
  map(interval_given_max_abs_err(max_err), vals)
end

interval_given_max_rel_err(
  val::F,
  max_err::Real,
  root_abs_err::Real) where {F <: AbstractFloat} =
  interval_given_max_abs_err(
    val,
    let a = abs(val)
      ifelse(floatmin(F) < a, max_err * a, root_abs_err)
    end)

interval_given_max_rel_err(max_err::Real, root_abs_err::Real) =
  let e = max_err, re = root_abs_err
    v -> interval_given_max_rel_err(v, e, re)
  end

function intervals_given_max_rel_err(
  vals::AbstractVector{<:AbstractFloat},
  max_err::Real,
  root_abs_err::Real = floatmin(Float64) * 128)

  (0 < max_err) || error("max error not positive")
  (max_err < 1) || error("max error not less than one")
  map(interval_given_max_rel_err(max_err, root_abs_err), vals)
end

# Vals must already be scaled and rounded to integers.
#
# The emulated lookup table is assumed to have unsigned elements, so
# the values must be nonnegative.
function intervals_for_fixed_point_lookup_table_emulation(
  vals::AbstractVector{F},
  safety_margin::AbstractFloat = 2.0f0^Int8(-32)) where {F <: AbstractFloat}

  all(v -> (0 <= v), vals) || error("negative values present")
  all(isinteger, vals) || error("not all values are integral")
  all(iszero, vals) && error("probable mistake detected, all values are zero")

  intervals_given_max_abs_err(
    map(v -> v + half(F), vals),
    half(F) - safety_margin)
end

intervaler_given_error_type(::AbsoluteError) =
  intervals_given_max_abs_err

intervaler_given_error_type(::RelativeError) =
  intervals_given_max_rel_err

intervals_given_max_err(
  vals::AbstractVector{<:AbstractFloat},
  error_type::NumericalErrorType,
  max_err::Real) =
  intervaler_given_error_type(error_type)(vals, max_err)

end
