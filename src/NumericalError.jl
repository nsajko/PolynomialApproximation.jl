# Copyright © 2023 Neven Sajko. All rights reserved.

module NumericalError

using
  MutableArithmetics,
  PolynomialPassingThroughIntervals.NumericalErrorTypes,
  ..DFloats, ..ScaledDFloats

export
  safe_absolute_error, safe_relative_error, safe_error_func

const AccurateType = Union{BigFloat, DFloat{F}} where {F <: AbstractFloat}

const AccurateTypeSc = Union{AccurateType{F}, ScaledDFloat{F}} where {F <: AbstractFloat}

function relative_error_check(::RelativeError, accur::AccurateType)
  iszero(accur) && error("division by zero")
  nothing
end

relative_error_check(::AbsoluteError, ::AccurateType) = nothing

# Theoretically, an integer like `true` should be the fastest choice,
# but I guess that requires improving Julia Base and
# MutableArithmetics.
#
# I think this is OK because the BigFloat representation of the number
# one is accurate independently of BigFloat precision.
const one1 = one(BigFloat)

absolute_error(accur::DFloat{T}, approx::T) where {T <: AbstractFloat} =
  DFloats.renormalized(abs(accur - approx))

relative_error(accur::DFloat{T}, approx::T) where {T <: AbstractFloat} =
  DFloats.renormalized(abs(DFloat(approx)/accur - one(T)))

error_func_df(::AbsoluteError) = absolute_error
error_func_df(::RelativeError) = relative_error

unsafe_error(et::NumericalErrorType, accur::DFloat{F}, approx::F) where
{F <: AbstractFloat} =
  error_func_df(et)(accur, approx)

# Modifies approx to contain the absolute error.
function absolute_error!(approx::F, accur::F) where {F <: BigFloat}
  operate!(-, approx, accur)
  operate!(abs, approx)
  nothing
end

# Modifies approx to contain the relative error.
function relative_error!(approx::F, accur::F) where {F <: BigFloat}
  # MutArithmetics.operate! for BigFloat division is not implemented yet
  local tmp = approx / accur
  operate!(zero, approx)
  operate!(+, approx, tmp)
  operate!(-, approx, one1)
  operate!(abs, approx)
  nothing
end

error_func_big(::AbsoluteError) = absolute_error!
error_func_big(::RelativeError) = relative_error!

function unsafe_error(et::NumericalErrorType, accur::BigFloat, approx::F) where
{F <: AbstractFloat}
  local app = BigFloat(zero(BigFloat) + approx)
  error_func_big(et)(app, accur)
  app
end

function safe_error_inner(
  et::NumericalErrorType,
  accur::AccurateType{F},
  approx::F) where {F <: AbstractFloat}

  isfinite(accur) || error("the accurate value is not finite")
  isfinite(approx) || (return typemax(F))
  relative_error_check(et, accur)
  unsafe_error(et, accur, approx)
end

function safe_error_inner_scaled(
  ::AbsoluteError,
  accur::ScaledDFloat{F},
  approx::F) where {F <: AbstractFloat}

  local app = ldexp(approx, -accur.e)
  local err = safe_error_inner(AbsoluteError(), accur.f, app)
  ldexp(err, accur.e)
end

function safe_error_inner_scaled(
  ::RelativeError,
  accur::ScaledDFloat{F},
  approx::F) where {F <: AbstractFloat}

  local app = ldexp(approx, -accur.e)
  local err = safe_error_inner(RelativeError(), accur.f, app)
  err
end

safe_error(
  et::NumericalErrorType,
  accur::ScaledDFloat{F},
  approx::AbstractFloat) where {F <: AbstractFloat} =
  safe_error_inner_scaled(et, accur, F(approx))

safe_error(
  et::NumericalErrorType,
  accur::DFloat{F},
  approx::AbstractFloat) where {F <: AbstractFloat} =
  safe_error_inner(et, accur, F(approx))

safe_error(
  et::NumericalErrorType,
  accur::BigFloat,
  approx::AbstractFloat) =
  safe_error_inner(et, accur, approx)

safe_absolute_error(accur::AccurateTypeSc, approx::AbstractFloat) =
  safe_error(AbsoluteError(), accur, approx)

safe_relative_error(accur::AccurateTypeSc, approx::AbstractFloat) =
  safe_error(RelativeError(), accur, approx)

safe_error_func(et::NumericalErrorType) =
  (accur::AccurateTypeSc, approx::AbstractFloat) -> safe_error(et, accur, approx)

end
