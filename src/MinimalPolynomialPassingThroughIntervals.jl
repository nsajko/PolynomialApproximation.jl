# Copyright © 2023 Neven Sajko. All rights reserved.

module MinimalPolynomialPassingThroughIntervals

using
  Tulip,
  PolynomialPassingThroughIntervals,
  PolynomialPassingThroughIntervals.NumericalErrorTypes,
  ..IntervalHelpers,
  Polynomials,
  ..CompressedPolynomials

import MathOptInterface

export minimal_polynomial_passing_through_intervals_easy

const PPI = PolynomialPassingThroughIntervals
const MOI = MathOptInterface

function lp_optimizer(::Type{F}, iter_limit::Int) where {F <: AbstractFloat}
  local lp = Tulip.Optimizer{F}()

  MOI.set(lp, MOI.RawOptimizerAttribute("IPM_IterationsLimit"), iter_limit)

  # Disabling presolve might be required when
  # F <: MultiFloats.MultiFloat
  #MOI.set(lp, MOI.RawOptimizerAttribute("Presolve_Level"), 0)

  lp
end

lp_optimizer_iteration_count(lp::Tulip.Optimizer{F}) where {F <: AbstractFloat} =
  MOI.get(lp, MOI.BarrierIterations())

function polynomial_passing_through_intervals(
  monomials::AbstractVector{<:Integer},
  dom::Vector{Rational{BigInt}},
  intervals::Vector{NTuple{2, BigFloat}},
  option::PPI.AbstractOption,
  ::Type{T},
  lp_iter_limit::Int) where {T <: AbstractFloat}

  local lp = lp_optimizer(T, lp_iter_limit)

  local coefs =
    polynomial_passing_through_intervals!(lp, monomials, dom, intervals, T, option)

  (coefficients = coefs, lp_iteration_count = lp_optimizer_iteration_count(lp))
end

struct OptimizationResult{
  VF <: AbstractVector{<:Real},
  VI <: AbstractVector{<:Integer},
  I <: Integer}

  polynomial_coefficients::VF
  monomials::VI
  lp_iteration_count::I
end

function minimal_polynomial_passing_through_intervals(
  monomials::Vector{<:Integer},
  dom::Vector{Rational{BigInt}},
  intervals::Vector{NTuple{2, BigFloat}},
  option::PPI.AbstractOption,
  lp_iter_limit::Int,
  ::Type{T}) where {T <: AbstractFloat}

  local len = 1

  local coefs = T[]

  local lp_iter_cnt = -1

  while true
    (length(monomials) < len) && (return OptimizationResult(coefs, monomials[1:0], -2))

    local coefs_itcnt = polynomial_passing_through_intervals(
      monomials[begin:(begin + len - 1)],
      dom,
      intervals,
      option,
      T,
      lp_iter_limit)
    coefs = coefs_itcnt.coefficients
    lp_iter_cnt = coefs_itcnt.lp_iteration_count

    isempty(coefs) || break

    len += 1
  end

  OptimizationResult(coefs, monomials[begin:(begin + len - 1)], lp_iter_cnt)
end

interval_intersection(itv1::NTuple{2, T}, itv2::NTuple{2, T}) where {T <: Any} =
  (max(first(itv1), first(itv2)), min(last(itv1), last(itv2)))

interval_intersections(itvs1, itvs2) =
  map(interval_intersection, itvs1, itvs2)

function minimal_polynomial_passing_through_intervals_easy(
  monomials::Vector{<:Integer},
  dom::Vector{Rational{BigInt}},
  intervals_for_intersection::Vector{NTuple{2, BigFloat}},
  accurate_values::Vector{BigFloat},
  max_error::AbstractFloat,
  error_type::NumericalErrorType = RelativeError(),
  option::PPI.ReplacedVariablesOption =
    PPI.ReplacedVariablesOption{:determinants}(),
  lp_iter_limit::Int = 2^11,
  ::Type{T} = BigFloat) where {T <: AbstractFloat}

  local coefs_monomials_lpitercnt = let x = (
    accurate_values,
    error_type,
    BigFloat(max_error))

    minimal_polynomial_passing_through_intervals(
      monomials,
      dom,
      interval_intersections(intervals_for_intersection, intervals_given_max_err(x...)),
      PPI.MinimaxOption{typeof(option)}(x...),
      lp_iter_limit,
      T)
  end

  (
    polynomial = Polynomial(reconstruct_coefficients_from_monomial_indices(
      coefs_monomials_lpitercnt.polynomial_coefficients,
      coefs_monomials_lpitercnt.monomials)),
    lp_iteration_count = coefs_monomials_lpitercnt.lp_iteration_count)
end

end
