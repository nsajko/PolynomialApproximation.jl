# Copyright © 2022 Neven Sajko. All rights reserved.

module TranslatedPolynomials

using Polynomials
using ..OverRing

export TranslatedPolynomial, offset_polynomial

# offset + p
struct TranslatedPolynomial{
  Coef <: Real,
  var,
  P <: AbstractPolynomial{Coef, var}} <: AbstractPolynomial{Coef, var}

  offset::Coef

  p::P

  TranslatedPolynomial{F, x, P}(o::F, p::P) where
  {F <: Real, x, P <: AbstractPolynomial{F, x}} =
    new{F, x, P}(o, p)
end

TranslatedPolynomial{F, x}(o::F, p::P) where
{F <: Real, x, P <: AbstractPolynomial{F, x}} =
  TranslatedPolynomial{F, x, P}(o, p)

TranslatedPolynomial{F}(o::F, p::P) where
{F <: Real, x, P <: AbstractPolynomial{F, x}} =
  TranslatedPolynomial{F, x}(o, p)

TranslatedPolynomial(o::F, p::P) where
{F <: Real, x, P <: AbstractPolynomial{F, x}} =
  TranslatedPolynomial{F}(o, p)

# Copy constructor
TranslatedPolynomial{F, x, P}(p::TranslatedPolynomial{F, x, P}) where
{F <: Real, x, P <: AbstractPolynomial{F, x}} =
  TranslatedPolynomial{F, x, P}(F(p.offset), P(p.p))

OverRing.over_ring(::Type{New}, p::TranslatedPolynomial{Old, x}) where
{New <: Real, Old <: Real, x} =
  TranslatedPolynomial{New, x}(New(p.offset), over_ring(New, p.p))

function Base.show(io::IO, p::TP) where {TP <: TranslatedPolynomial}
  print(io, TP, "(")
  show(io, p.offset)
  print(io, " + ")
  show(io, p.p)
  print(io, ")")
end

Base.show(io::IO, ::MIME"text/plain", p::TranslatedPolynomial) =
  show(io, p)

Polynomials.degree(p::TranslatedPolynomial) = degree(p.p)

Base.length(p::TranslatedPolynomial) = length(p.p) + 1

Base.zero(::Type{TP}) where
{v, P <: AbstractPolynomial, TP <: TranslatedPolynomial{<:Any, v, P}} =
  TP(false, zero(P))

Base.one(::Type{TP}) where
{v, P <: AbstractPolynomial, TP <: TranslatedPolynomial{<:Any, v, P}} =
  TP(false, one(P))

Base.:+(c::F, p::TP) where {F <: Real, TP <: TranslatedPolynomial{F}} =
  TP(p.offset, c + p)

Base.:+(p::TP, c::F) where {F <: Real, TP <: TranslatedPolynomial{F}} = c + p

Base.:+(l::TP, r::TP) where {TP <: TranslatedPolynomial} =
  TP(l.offset + r.offset, l.p + r.p)

Base.evalpoly(x::Real, p::TranslatedPolynomial) =
  p.offset + evalpoly(x, p.p)

offset_polynomial(offset::F, pol::AbstractPolynomial{F, v}) where {F <: Real, v} =
  TranslatedPolynomial{F, v, Polynomial{F, v}}(offset, pol - offset)

end
