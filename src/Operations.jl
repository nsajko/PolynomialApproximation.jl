# Copyright © 2023 Neven Sajko. All rights reserved.

module Operations

using ..ValueTypes

export Operation

# TODO: see the ValueTypes.jl TODO comment

struct Operation
  op::Int8

  function Operation(op::Integer)
    (0 <= op < 4) || error("unsupported operation")
    new(op)
  end
end

"""
A floating-point constant
"""
const fpc = Operation(0)

"""
Addition
"""
const add = Operation(1)

"""
Multiplication
"""
const mul = Operation(2)

"""
Fused multiply-add (FMA)
"""
const fma = Operation(3)

const operation_types_operands = (
  fpc => [ValueTypes.fpc],
  add => [ValueTypes.name for i in 1:2],
  mul => [ValueTypes.name for i in 1:2],
  fma => [ValueTypes.name for i in 1:3])

const operation_operands = Dict{Operation, Vector{ValueType}}(operation_types_operands)

operand_types(op::Operation) = operation_operands[op]

arity(op::Operation) = length(operand_types(op))

end
