# Copyright © 2023 Neven Sajko. All rights reserved.

module FloatingPointConstants

using ..AbstractValues, ..ValueTypes, ..Values

export FloatingPointConstant

struct FloatingPointConstant <: AbstractValue
  c::Value

  FloatingPointConstant(c::AbstractFloat) = new(Value(string(c)))
end

AbstractValues.value(c::FloatingPointConstant) = value(c.c)

ValueTypes.value_type(::FloatingPointConstant) = ValueTypes.fpc

end
