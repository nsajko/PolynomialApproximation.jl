# Copyright © 2023 Neven Sajko. All rights reserved.

module MFloats

export MFloat

# TODO: add renormalize from MultiFloats?

is_small(x::AbstractFloat) = iszero(x) | issubnormal(x)

# This type is mathematically similar to MultiFloats.MultiFloat, but
# serves a different purpose: code generation.
#
# The elements should be sorted by signicance in descending order.
struct MFloat{T <: AbstractFloat}
  f::Vector{T}

  function MFloat{T}(f::Vector{T}) where {T <: AbstractFloat}
    any(!isfinite, f) && error("an MFloat must be finite")

    any(is_small, f) && error("all terms of an MFloat must be nonzero")

    issorted(f, by = abs, rev = true) ||
      error("the elements must be sorted by significance in descending order")

    for i in 0:(length(f) - 2)
      local hi = f[begin + i]
      local lo = f[begin + i + 1]
      ==(hi, hi + lo) || error("magnitude too great: $lo")
    end

    new{T}(f)
  end
end

function MFloat{T}(x::Real) where {T <: AbstractFloat}
  isfinite(x) || error("given number not finite")

  local ret = T[]
  local tmp = T(x)

  isfinite(tmp) || error("given number doesn't fit in given type")

  if !is_small(tmp)
    push!(ret, tmp)
    while true
      x -= last(ret)
      local t = T(x)
      is_small(t) && break
      push!(ret, t)
    end
  end
  MFloat{T}(ret)
end

end
