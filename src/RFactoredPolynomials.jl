# Copyright © 2022 Neven Sajko. All rights reserved.

# Polynomial factorization over the reals

module RFactoredPolynomials

using
  Polynomials, ..OverRing,
  PolynomialPassingThroughIntervals.NumericalErrorTypes,
  ..NumericalError

export RFactoredPolynomial

struct RFactoredPolynomial{Coef <: Real, var} <: AbstractPolynomial{Coef, var}
  constant_factor::Coef
  linear_factors::Vector{ImmutablePolynomial{Coef, var, 2}}
  quadratic_factors::Vector{ImmutablePolynomial{Coef, var, 3}}
end

# Copy constructor
RFactoredPolynomial{F, x}(p::RFactoredPolynomial{F, x}) where {F <: Real, x} =
  RFactoredPolynomial{F, x}(
    F(p.constant_factor),
    copy(p.linear_factors),
    copy(p.quadratic_factors))

OverRing.over_ring(::Type{New}, p::RFactoredPolynomial{Old, x}) where
{New <: Real, Old <: Real, x} =
  let ringer = (ip -> over_ring(New, ip))
    RFactoredPolynomial{New, x}(
      New(p.constant_factor),
      map(ringer, p.linear_factors),
      map(ringer, p.quadratic_factors))
  end

Base.show(io::IO, p::FP) where {FP <: RFactoredPolynomial} =
  print(
    io,
    FP, "(constant factor: ", p.constant_factor, ", linear factor count: ",
    length(p.linear_factors), ", quadratic factor count: ", length(p.quadratic_factors),
    ")")

Base.show(io::IO, ::MIME"text/plain", p::RFactoredPolynomial) =
  show(io, p)

Polynomials.degree(p::RFactoredPolynomial) =
  length(p.linear_factors) + 2*length(p.quadratic_factors)

# Assuming the non-constant factors are monic.
Base.length(p::RFactoredPolynomial) = degree(p) + 1

Base.zero(::Type{FP}) where {F <: Real, var, FP <: RFactoredPolynomial{F, var}} =
  FP(zero(F), ImmutablePolynomial{F, var, 2}[], ImmutablePolynomial{F, var, 3}[])

Base.one(::Type{FP}) where {F <: Real, var, FP <: RFactoredPolynomial{F, var}} =
  FP(one(F), ImmutablePolynomial{F, var, 2}[], ImmutablePolynomial{F, var, 3}[])

Base.:*(l::F, r::FP) where {F <: Real, var, FP <: RFactoredPolynomial{F, var}} =
  let c = l * r.constant_factor
    iszero(c) ?
      zero(FP) :
      FP(c, copy(r.linear_factors), copy(r.quadratic_factors))
  end

Base.:*(l::FP, r::F) where {F <: Real, var, FP <: RFactoredPolynomial{F, var}} = r * l

Base.:*(l::FP, r::FP) where {F <: Real, var, FP <: RFactoredPolynomial{F, var}} =
  let c = l.constant_factor * r.constant_factor
    iszero(c) ?
      zero(FP) :
      FP(
        c,
        vcat(l.linear_factors, r.linear_factors),
        vcat(l.quadratic_factors, r.quadratic_factors))
  end

factors_to_poly(factors::AbstractVector{<:ImmutablePolynomial{F, var}}) where
{F <: Real, var} =
  mapreduce(Polynomial{F, var}, *, factors, init = one(Polynomial{F, var}))

Polynomials.Polynomial{F, var}(p::FP) where
{F <: Real, var, FP <: RFactoredPolynomial{F, var}} =
  factors_to_poly(p.quadratic_factors) *
  factors_to_poly(p.linear_factors) *
  p.constant_factor

Polynomials.Polynomial(p::FP) where {F <: Real, var, FP <: RFactoredPolynomial{F, var}} =
  Polynomial{F, var}(p)

evaluate_poly(x::Real) =
  let x = x
    p -> evalpoly(x, p)
  end

map_eval_poly(x::Real, polys::AbstractVector{<:AbstractPolynomial}) =
  map(evaluate_poly(x), polys)

function reduce_binary_tree(op::Op, a::AbstractVector{T}) where
{Op <: Function, T <: Number}
  while 2 < length(a)
    local b = T[]
    for i in 0:2:(length(a) - 2)
      push!(b, op(a[begin + i], a[begin + i + 1]))
    end
    isodd(length(a)) && push!(b, last(a))
    a = b
  end

  ==(length(a), 2) || error("unexpected")

  (first(a), last(a))
end

reduce_binary_tree_mul(a::AbstractVector{<:Number}) = reduce_binary_tree(*, a)

function evaluated_factors(x::Real, p::RFactoredPolynomial)
  local f = (
    map_eval_poly(x, p.quadratic_factors),
    map_eval_poly(x, p.linear_factors))

  # The order may matter for performance in the generated code, and we
  # need to be in line with the generated code.
  <(map(length, f)...) && (f = reverse(f))

  local factors = vcat(f...)

  isone(p.constant_factor) || push!(factors, p.constant_factor)

  factors
end

Base.evalpoly(x::Real, p::RFactoredPolynomial) =
  *(reduce_binary_tree_mul(evaluated_factors(x, p))...)

real_root_to_linear_polynomial(r::F, ::Val{var}) where {F <: Real, var} =
  ImmutablePolynomial{F, var, 2}((-r, one(F)))

root_pair_to_quadratic_polynomial(r1::C, r2_conj::C, ::Val{var}) where
{F <: Real, C <: Complex{F}, var} =
  let r2 = conj(r2_conj)
    ImmutablePolynomial{F, var, 3}((real(r1 * r2), -real(r1 + r2), one(F)))
  end

real_root_to_linear_polynomial(::Val{var}) where {var} =
  r -> real_root_to_linear_polynomial(r, Val{var}())

root_pair_to_quadratic_polynomial(::Val{var}) where {var} =
  (l, r) -> root_pair_to_quadratic_polynomial(l, r, Val{var}())

Polynomials.fromroots(
  ::Type{FP},
  roots::AbstractVector{F}) where {F <: Real, var, FP <: RFactoredPolynomial{F, var}} =
  FP(
    one(F),
    map(real_root_to_linear_polynomial(Val{var}()), roots),
    map(root_pair_to_quadratic_polynomial(Val{var}()), Complex{F}[]))

function nextfl(x::AbstractFloat, ::Val{m}) where {m}
  if 0 <= m
    for i in 1:m
      x = nextfloat(x)
    end
  else
    for i in 1:-m
      x = prevfloat(x)
    end
  end
  x
end

is_almost_zero(x::Rational) = iszero(x)
is_almost_zero(x::AbstractFloat) = abs(x) <= nextfl(floatmin(x), Val{10}())

relative_error(accurate::Real, approx::Real) =
  NumericalError.unsafe_error(RelativeError(), accurate, approx)

max_rel_error(::Type{F}) where {F <: AbstractFloat} =
  40 * eps(F)

are_almost_equal(x::Rational, y::Rational) = x == y

are_almost_equal(x::F, y::F) where {F <: AbstractFloat} =
  let m = max_rel_error(F)
    (relative_error(x, y) <= m) & (relative_error(y, x) <= m)
  end

are_almost_equal(t::NTuple{2}) = are_almost_equal(t...)

are_almost_equal(x::C, y::C) where {F <: Real, C <: Complex{F}} =
  # TODO: hypot?
  all(are_almost_equal, (map(real, (x, y)), map(imag, (x, y))))

# Classifies complex numbers by the sign of the imaginary part.
function partition_by_sign(x::AbstractVector{<:Complex})
  local lt = Int32[]
  local gt = Int32[]
  local eq = Int32[]
  for i in eachindex(x)
    local im = imag(x[i])
    if is_almost_zero(im)
      push!(eq, i)
    elseif im < 0
      push!(lt, i)
    else
      push!(gt, i)
    end
  end
  (lt = lt, gt = gt, eq = eq)
end

function Polynomials.fromroots(
  ::Type{FP},
  roots::AbstractVector{<:Complex{F}}) where
{F <: Real, var, FP <: RFactoredPolynomial{F, var}}
  local clas = partition_by_sign(roots)

  let ll = map(length, (clas.lt, clas.gt))
    ==(ll...) ||
      error("can't form conjugate pairs: ",
            first(ll), " != ", last(ll), ",  ", length(clas.eq))
  end

  local real_roots = map(real, roots[clas.eq])

  local p = roots[clas.gt]
  local n = map(conj, roots[clas.lt])

  for v in (p, n)
    sort!(v, by = ((x::Complex{<:Real}) -> (real(x), imag(x))))
  end

  any(!are_almost_equal, zip(p, n)) &&
    error("can't match conjugate pairs")

  FP(
    one(F),
    map(real_root_to_linear_polynomial(Val{var}()), real_roots),
    map(root_pair_to_quadratic_polynomial(Val{var}()), p, n))
end

Polynomials.fromroots(
  ::Type{RFactoredPolynomial},
  roots::AbstractVector{F};
  var::Symbol = :x) where {F <: Real} =
  fromroots(RFactoredPolynomial{F, var}, roots)

Polynomials.fromroots(
  ::Type{RFactoredPolynomial},
  roots::AbstractVector{<:Complex{F}};
  var::Symbol = :x) where {F <: Real} =
  fromroots(RFactoredPolynomial{F, var}, roots)

end
