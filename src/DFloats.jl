# Copyright © 2023 Neven Sajko. All rights reserved.

module DFloats

export DFloat

# See "Tight and Rigorous Error Bounds for Basic Building Blocks of
# Double-Word Arithmetic" by Joldes, Muller, Popescu

is_small(x::AbstractFloat) = iszero(x) | issubnormal(x)

# Double-word floating point number
#
# TODO: should this subtype Real? It should NOT subype AbstractFloat.
struct DFloat{T <: AbstractFloat}
  hi::T
  lo::T
end

DFloat{T}(x::T) where {T <: AbstractFloat} = DFloat{T}(x, zero(T))

DFloat(x::T) where {T <: AbstractFloat} = DFloat{T}(x)

limbs(x::DFloat) = (x.hi, x.lo)

function check(x::DFloat)
  local d = limbs(x)
  local (hi, lo) = d

  any(!isfinite, d) && error("a DFloat must be finite")

  issorted(d, by = abs, rev = true) ||
    error("the elements must be sorted by significance in descending order")

  ==(hi, hi + lo) || error("magnitude too great: $lo")

  nothing
end

Base.BigFloat(f::DFloat) = +(map(BigFloat, limbs(f))...)

Base.isfinite(f::DFloat) = all(isfinite, limbs(f))

Base.iszero(f::DFloat) = all(iszero, limbs(f))

Base.isone(f::DFloat) = isone(f.hi) & iszero(f.lo)

Base.:<(l::DFloat{T}, r::DFloat{T}) where {T <: AbstractFloat} =
  <(l.hi, r.hi) | (==(l.hi, r.hi) & <(l.lo, r.lo))

Base.zero(::Type{DFloat{T}}) where {T <: AbstractFloat} = DFloat(zero(T), zero(T))

Base.one(::Type{DFloat{T}}) where {T <: AbstractFloat} = DFloat(one(T), zero(T))

"""
Fast2Sum

The exponent of a must be at least as great as the exponent of b.

This holds, for example, when abs(b) ≤ abs(a).
"""
function fast_two_sum(a::T, b::T) where {T <: AbstractFloat}
  local s = a + b
  local z = s - a
  local t = b - z
  DFloat(s, t)
end

renormalized(x::DFloat) = fast_two_sum(x.hi, x.lo)

Base.AbstractFloat(x::DFloat) = renormalized(x).hi

"""
2Sum
"""
function two_sum(a::T, b::T) where {T <: AbstractFloat}
  local s  = a + b
  local a′ = s - b
  local b′ = s - a′
  local δa = a - a′
  local δb = b - b′
  local t  = δa + δb
  DFloat(s, t)
end

"""
2Prod using FMA
"""
function two_prod(a::T, b::T) where {T <: AbstractFloat}
  local π = a * b
  local ρ = fma(a, b, -π)
  DFloat(π, ρ)
end

Base.:-(f::DFloat) = DFloat(map(-, limbs(f))...)

Base.abs(f::DFloat) = (f.hi < 0 ? -f : f)

# Algorithm 4 in the paper.
function Base.:+(x::DFloat{T}, y::T) where {T <: AbstractFloat}
  local s = two_sum(x.hi, y)
  local v = x.lo + s.lo
  local z = fast_two_sum(s.hi, v)
  z
end

Base.:+(b::T, a::DFloat{T}) where {T <: AbstractFloat} = a + b

Base.:-(a::DFloat{T}, b::T) where {T <: AbstractFloat} = a + -b

Base.:-(b::T, a::DFloat{T}) where {T <: AbstractFloat} = -(a - b)

# Algorithm 6 in the paper.
function Base.:+(x::DFloat{T}, y::DFloat{T}) where {T <: AbstractFloat}
  local s = two_sum(x.hi, y.hi)
  local t = two_sum(x.lo, y.lo)
  local c = s.lo + t.hi
  local v = fast_two_sum(s.hi, c)
  local w = t.lo + v.lo
  local z = fast_two_sum(v.hi, w)
  z
end

Base.:-(a::DFloat{T}, b::DFloat{T}) where {T <: AbstractFloat} = a + -b

# Algorithm 9 in the paper.
function Base.:*(x::DFloat{T}, y::T) where {T <: AbstractFloat}
  local c = two_prod(x.hi, y)
  local c_l3 = fma(x.lo, y, c.lo)
  local z = fast_two_sum(c.hi, c_l3)
  z
end

Base.:*(b::T, a::DFloat{T}) where {T <: AbstractFloat} = a * b

# Algorithm 12 in the paper.
function Base.:*(x::DFloat{T}, y::DFloat{T}) where {T <: AbstractFloat}
  local c = two_prod(x.hi, y.hi)
  local t_l0 = x.lo * y.lo
  local t_l1 = fma(x.hi, y.lo, t_l0)
  local c_l2 = fma(x.lo, y.hi, t_l1)
  local c_l3 = c.lo + c_l2
  local z = fast_two_sum(c.hi, c_l3)
  z
end

# Algorithm 15 in the paper.
function Base.:/(x::DFloat{T}, y::T) where {T <: AbstractFloat}
  local t_hi = x.hi / y
  local π = two_prod(t_hi, y)
  local δh = x.hi - π.hi
  local δt = δh - π.lo
  local δ = δt + x.lo
  local t_lo = δ / y
  local z = fast_two_sum(t_hi, t_lo)
  z
end

# Algorithm 18 in the paper.
function Base.:/(x::DFloat{T}, y::DFloat{T}) where {T <: AbstractFloat}
  local t_hi = inv(y.hi)
  local r_hi = -fma(y.hi, t_hi, -one(T))
  local r_lo = -y.lo * t_hi
  local e = fast_two_sum(r_hi, r_lo)
  local δ = e * t_hi
  local m = δ + t_hi
  local z = x * m
  z
end

Base.inv(f::DFloat) = one(f) / f

function DFloat{T}(x::Real) where {T <: AbstractFloat}
  isfinite(x) || error("given number not finite")

  local hi = T(x)
  local lo = T(x - hi)

  local ret = renormalized(DFloat(hi, lo))
  check(ret)
  ret
end

end
