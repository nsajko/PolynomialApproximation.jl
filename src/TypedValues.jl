# Copyright © 2023 Neven Sajko. All rights reserved.

module TypedValues

using
  ..AbstractValues, ..ValueTypes, ..Values,
  ..Names, ..FloatingPointConstants

export TypedValue

struct TypedValue{name_sep} <: AbstractValue
  t::ValueType
  v::Value

  TypedValue{ns}(v::AbstractValue) where {ns} = new{ns}(value_type(v), value(v))
end

AbstractValues.value(v::TypedValue) = value(v.v)

ValueTypes.value_type(v::TypedValue) = v.t

TypedValue{ns}(v::AbstractFloat) where {ns} = TypedValue{ns}(FloatingPointConstant(v))

end
