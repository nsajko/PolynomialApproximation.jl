# Copyright © 2023 Neven Sajko. All rights reserved.

module FPConstMFloats

using ..FloatingPointConstants

export FPConstMFloat

struct FPConstMFloat
  f::Vector{FloatingPointConstant}
end

end
