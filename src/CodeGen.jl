# Copyright © 2023 Neven Sajko. All rights reserved.

module CodeGen

using
  Polynomials, ..TranslatedPolynomials, ..RFactoredPolynomials, ..CompressedPolynomials,
  ..AbstractValues, ..ValueTypes, ..Names, ..FloatingPointConstants, ..TypedValues,
  ..NameMFloats,
  ..Operations, ..Assignments

export
  implement_polynomial_as_program,
  implement_exponentiation

function implement_exponentiation(exp::Int, input::Name{ns}, tmp::Name{ns}) where {ns}
  local prog = Assignment{ns}[]

  if exp == 0
    tmp = Name(tmp, "exp0")
    prog = [Assignment(tmp, 1.0)]
  elseif exp == 1
  elseif exp == 2
    tmp = Name(tmp, "exp2")
    prog = [Assignment(tmp, Operations.mul, [input, input])]
  elseif exp == 3
    tmp = Name(tmp, "exp3")
    local square = Name(tmp, "sqr")
    prog = [
      Assignment(square, Operations.mul, [input, input]),
      Assignment(Name(tmp, "ret"), Operations.mul, [square, input])]
  elseif exp == 4
    tmp = Name(tmp, "exp4")
    local square = Name(tmp, "sqr")
    prog = [
      Assignment(square, Operations.mul, [input, input]),
      Assignment(Name(tmp, "ret"), Operations.mul, [square, square])]
  else
    # TODO: implement the general case using exponentiation by squaring
    #       with signed-digit recoding/addition-subtraction chains
    error("exponent $exp not implemented yet")
  end

  local result = if isempty(prog)
    input
  else
    last(prog).name
  end

  (prog, result)
end

"""
Converts a polynomial into a vector of assignments representing a
program.

First argument: polynomial

Second argument: input name

Third argument: prefix for local names (namespace)

Return value: two-element tuple:

  * the generated program, an ordered collection of Assignment objects

  * the names holding the program's return value, a NameMFloat object
"""
implement_polynomial_as_program

# Linear polynomial.
function implement_polynomial_as_program(
  p::ImmutablePolynomial{<:AbstractFloat, unused, 2},
  input::Name{ns},
  tmp::Name{ns}) where {unused, ns}

  local coef = coeffs(p)

  # Names
  local c0 = Name(tmp, "c0")
  local c1 = Name(tmp, "c1")
  local out = Name(tmp, "out")

  local prog = (isone(last(coef)) ?

    # Monic polynomial
    [
      Assignment(c0, coef[begin + 0]),
      Assignment(out, Operations.add, [input, c0])] :

    [
      Assignment(c0, coef[begin + 0]),
      Assignment(c1, coef[begin + 1]),
      Assignment(out, Operations.fma, [input, c1, c0])])

  (prog, NameMFloat([out]))
end

# Quadratic polynomial.
function implement_polynomial_as_program(
  p::ImmutablePolynomial{<:AbstractFloat, unused, 3},
  input::Name{ns},
  tmp::Name{ns}) where {unused, ns}

  local coef = coeffs(p)

  # Names
  local c0 = Name(tmp, "c0")
  local c1 = Name(tmp, "c1")
  local c2 = Name(tmp, "c2")
  local t = Name(tmp, "t")
  local out = Name(tmp, "out")

  local prog = (isone(last(coef)) ?

    # Monic polynomial
    [
      Assignment(c0, coef[begin + 0]),
      Assignment(c1, coef[begin + 1]),
      Assignment(t, Operations.add, [input, c1]),
      Assignment(out, Operations.fma, [input, t, c0])] :

    [
      Assignment(c0, coef[begin + 0]),
      Assignment(c1, coef[begin + 1]),
      Assignment(c2, coef[begin + 2]),
      Assignment(t, Operations.fma, [input, c2, c1]),
      Assignment(out, Operations.fma, [input, t, c0])])

  (prog, NameMFloat([out]))
end

function implement_polynomial_as_program(
  p::Polynomial{<:AbstractFloat},
  input::Name{ns},
  tmp::Name{ns}) where {ns}

  local coef = coeffs(p)

  local coef_name = let t = tmp
    i -> Name(t, string("c", i))
  end

  local prog = Assignment{ns}[
    Assignment(coef_name(i), coef[begin + i])
    for i in 0:(length(coef) - 1)]

  local temp_name = let t = tmp
    i -> Name(t, string("t", i))
  end

  if isempty(coef)
    error("TODO: should this be the zero polynomial?")
  elseif isone(length(coef))
  else
    push!(
      prog,
      Assignment(
        temp_name(length(coef) - 2),
        Operations.fma,
        [input, coef_name(length(coef) - 1), coef_name(length(coef) - 2)]))

    for i in (length(coef) - 3):-1:0
      push!(
        prog,
        Assignment(temp_name(i), Operations.fma, [input, temp_name(i + 1), coef_name(i)]))
    end
  end

  (prog, NameMFloat([last(prog).name]))
end

function implement_polynomial_as_program(
  p::CompressedPolynomial,
  input::Name{ns},
  tmp::Name{ns}) where {ns}

  # Names
  local y = Name(tmp, "y")
  local z = Name(tmp, "z")
  local t = Name(tmp, "t")

  local (prog_y, ry) = implement_exponentiation(Int(p.k), input, y)
  local (prog_z, rz) = implement_exponentiation(Int(p.l), input, z)

  local prog = vcat(prog_y, prog_z)

  local (prog_inner, r) = implement_polynomial_as_program(p.main_factor, ry, t)

  append!(prog, prog_inner)

  push!(prog, Assignment(Name(tmp, "out"), Operations.mul, [rz, only(r.f)]))

  (prog, NameMFloat([last(prog).name]))
end

function implement_polynomials(
  ps::AbstractVector{<:AbstractPolynomial},
  input::Name{sep},
  tmp::Name{sep}) where {sep}

  local prog = Assignment{sep}[]
  local return_values = NameMFloat{sep}[]

  for i in 0:(length(ps) - 1)
    local (prog_tmp, ret_tmp) = implement_polynomial_as_program(
      ps[begin + i], input, Name(tmp, string("p", i)))
    append!(prog, prog_tmp)
    push!(return_values, ret_tmp)
  end

  (prog, return_values)
end

function implement_factor_evaluation(
  p::RFactoredPolynomial,
  input::Name{sep},
  tmp::Name{sep}) where {sep}

  # Linear factors
  local (prog_lin, factors_lin) =
    implement_polynomials(p.linear_factors, input, Name(tmp, "linearfactor"))

  # Quadratic factors
  local (prog_quad, factors_quad) =
    implement_polynomials(p.quadratic_factors, input, Name(tmp, "quadraticfactor"))

  local prog = Assignment{sep}[]
  local factors_multi = NameMFloat{sep}[]

  # Join the linear and quadratic factors.
  if length(prog_lin) < length(prog_quad)
    append!(prog, prog_quad, prog_lin)
    factors_multi = vcat(factors_quad, factors_lin)
  else
    append!(prog, prog_lin, prog_quad)
    factors_multi = vcat(factors_lin, factors_quad)
  end

  local factors = map(nf -> only(nf.f), factors_multi)

  # Constant factor
  if !isone(p.constant_factor)
    local c = Name(tmp, "constantfactor")
    push!(prog, Assignment(c, p.constant_factor))
    push!(factors, c)
  end

  (prog, factors)
end

function reduction_step(factors::Vector{Name{sep}}, tmp::Name{sep}) where {sep}
  local prog = Assignment{sep}[]
  local new_factors = Name{sep}[]

  for i in 0:2:(length(factors) - 2)
    local new_factor = Name(tmp, string("f", i))
    push!(
      prog,
      Assignment(
        new_factor, Operations.mul, [factors[begin + i], factors[begin + i + 1]]))
    push!(new_factors, new_factor)
  end

  isodd(length(factors)) && push!(new_factors, last(factors))

  (prog, new_factors)
end

function multiplication_reduce_tree(factors::Vector{Name{sep}}, tmp::Name{sep}) where
{sep}
  local prog = Assignment{sep}[]

  local i = 0

  while 2 < length(factors)
    local (prog_step, new_factors) = reduction_step(factors, Name(tmp, string("fac", i)))
    append!(prog, prog_step)
    factors = new_factors
    i += 1
  end

  ==(length(factors), 2) || error("unexpected")

  (prog, (first(factors), last(factors)))
end

function implement_polynomial_as_program(
  p::RFactoredPolynomial,
  input::Name{sep},
  tmp::Name{sep}) where {sep}

  # Compute the factors.
  local (prog, factors) = implement_factor_evaluation(p, input, Name(tmp, "factorevaluation"))

  # Multiply the factors together.
  local (prog_reduction, last_two_factors) =
    multiplication_reduce_tree(factors, Name(tmp, "multiplicationreducetree"))
  append!(prog, prog_reduction)
  push!(prog, Assignment(Name(tmp, "out"), Operations.mul, [last_two_factors...]))

  (prog, NameMFloat([last(prog).name]))
end

function implement_polynomial_as_program(
  tp::TranslatedPolynomial{<:Real, unused, <:RFactoredPolynomial},
  input::Name{sep},
  tmp::Name{sep}) where {unused, sep}

  local p = tp.p

  # Compute the factors.
  local (prog, factors) =
    implement_factor_evaluation(p, input, Name(tmp, "factorevaluation"))

  # Multiply the factors together.
  local (prog_reduction, last_two_factors) =
    multiplication_reduce_tree(
      factors,
      Name(tmp, "multiplicationreducetree"))
  append!(prog, prog_reduction)
  local t = Name(tmp, "t")
  push!(prog, Assignment(t, tp.offset))
  push!(prog, Assignment(Name(tmp, "out"), Operations.fma, [last_two_factors..., t]))

  (prog, NameMFloat([last(prog).name]))
end

end
