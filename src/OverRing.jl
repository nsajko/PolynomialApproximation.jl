# TODO: this should live in Polynomials.jl

module OverRing

using Polynomials

export over_ring

over_ring(::Type{New}, p::Polynomial{Old, x}) where {New <: Number, Old <: Number, x} =
  Polynomial{New, x}(map(New, coeffs(p)))

over_ring(::Type{New}, p::ImmutablePolynomial{Old, x, n}) where
{New <: Number, Old <: Number, x, n} =
  ImmutablePolynomial{New, x, n}(map(New, coeffs(p)))

end
