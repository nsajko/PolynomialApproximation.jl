# Copyright © 2023 Neven Sajko. All rights reserved.

module Names

using ..AbstractValues, ..ValueTypes, ..Values

export Name

struct Name{sep} <: AbstractValue
  v::Value

  function Name{sep}(n::Value) where {sep}
    isa(sep, Symbol) ||
      error("type parameter must be a Symbol")
    ('0' <= first(string(n)) <= '9') &&
      error("a name is not allowed to start with a digit")
    new{sep}(n)
  end
end

Name{s}(n::AbstractString) where {s} = Name{s}(Value(n))

Name(n::AbstractString) = Name{:_}(n)

AbstractValues.value(n::Name) = value(n.v)

ValueTypes.value_type(::Name) = ValueTypes.name

# Namespacing
Name(l::Name{s}, r::Name{s}) where {s} = Name{s}(string(l, s, r))
Name(l::Name{s}, r::AbstractString)  where {s} = Name(l, Name{s}(r))

end
