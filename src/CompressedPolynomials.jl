# Copyright © 2022 Neven Sajko. All rights reserved.

module CompressedPolynomials

using Polynomials, ..OverRing

export
  CompressedPolynomial,
  reconstruct_coefficients_from_monomial_indices

# The polynomial is equal to var_x^l * main_factor, where the latter is
# a polynomial in var_y.
struct CompressedPolynomial{
  Coef <: Real,
  var_x,
  var_y,
  P <: AbstractPolynomial{Coef, var_y}} <: AbstractPolynomial{Coef, var_x}

  # Multiplicity of the zero root
  l::Int32

  # Change of variable: var_y = var_x^k
  k::Int32

  # main factor
  main_factor::P

  function CompressedPolynomial{F, x, y, P}(l::Integer, k::Integer, p::P) where
  {x, y, F <: Real, P <: AbstractPolynomial{F, y}}
    isa(x, Symbol) || error("first var is not a Symbol")
    isa(y, Symbol) || error("second var is not a Symbol")
    ===(x, y) && error("variables must be different")
    new{F, x, y, P}(l, k, p)
  end
end

CompressedPolynomial{F, x, y}(l::Integer, k::Integer, p::P) where
{x, y, F <: Real, P <: AbstractPolynomial{F, y}} =
  CompressedPolynomial{F, x, y, P}(l, k, p)

CompressedPolynomial{F, x}(l::Integer, k::Integer, p::P) where
{x, y, F <: Real, P <: AbstractPolynomial{F, y}} =
  CompressedPolynomial{F, x, y}(l, k, p)

# Copy constructor
CompressedPolynomial{F, x, y, P}(p::CompressedPolynomial{F, x, y, P}) where
{x, y, F <: Real, P <: AbstractPolynomial{F, y}} =
  CompressedPolynomial{F, x}(p.l, p.k, P(p.main_factor))

OverRing.over_ring(::Type{New}, p::CompressedPolynomial{Old, x}) where
{New <: Real, Old <: Real, x} =
  CompressedPolynomial{New, x}(p.l, p.k, over_ring(New, p.main_factor))

function Base.show(io::IO, p::CP) where
{var_x, var_y, CP <: CompressedPolynomial{<:Any, var_x, var_y}}
  print(
    io,
    CP, "(change of variable: ", var_y, " = ", var_x, "^", p.k, ", function: ", var_x,
    "^", p.l, " * ")
  show(io, p.main_factor)
  print(io, ")")
end

Base.show(io::IO, ::MIME"text/plain", p::CompressedPolynomial) =
  show(io, p)

Polynomials.degree(p::CompressedPolynomial) = p.l + p.k * degree(p.main_factor)

Base.length(p::CompressedPolynomial) = length(p.main_factor)

Base.zero(::Type{CP}) where
{x, y, P <: AbstractPolynomial, CP <: CompressedPolynomial{<:Any, x, y, P}} =
  CP(false, gcd(false), zero(P))

Base.one(::Type{CP}) where
{x, y, P <: AbstractPolynomial, CP <: CompressedPolynomial{<:Any, x, y, P}} =
  CP(false, gcd(false), one(P))

function Base.evalpoly(x::Real, p::CompressedPolynomial)
  local y = x^(p.k)
  x^(p.l) * evalpoly(y, p.main_factor)
end

(p::CompressedPolynomial)(x::Real) = Base.evalpoly(x, p)

function reconstruct_coefficients_from_monomial_indices(
  coefs::AbstractVector{F},
  monomials::AbstractVector) where {F <: Any}

  local ret = F[]

  local j = firstindex(coefs)

  for i in 0:last(monomials)
    c = zero(F)
    if i in monomials
      c = coefs[j]
      j += 1
    end
    push!(ret, c)
  end

  ret
end

function CompressedPolynomial{F, var_x, var_y, Polynomial{F, var_y}}(
  coefs::AbstractVector{F},
  monomials::AbstractVector{<:Integer}) where {F <: Real, var_x, var_y}

  ==(map(length, (coefs, monomials))...) || error("length mismatch")

  isempty(monomials) && (
    # zero polynomial
    return zero(CompressedPolynomial{F, var_x, var_y, Polynomial{F, var_y}}))

  local l = first(monomials)

  local subtracter = let l = l
    x -> x - l
  end

  monomials = map(subtracter, monomials)

  local k = gcd(monomials)

  local divider = let k = k
    x -> div(x, k)
  end

  monomials = map(divider, monomials)

  CompressedPolynomial{F, var_x, var_y, Polynomial{F, var_y}}(
    l,
    k,
    Polynomial{F, var_y}(reconstruct_coefficients_from_monomial_indices(
      coefs,
      monomials)))
end

function nonzero_monomials(coefs::AbstractVector, ::Type{T}) where {T <: Integer}
  local ret = T[]

  for i in map(T, 0:(length(coefs) - 1))
    iszero(coefs[begin + i]) || push!(ret, i)
  end

  ret
end

adder(n::Number) =
  let n = n
    x -> x + n
  end

function polynomial_to_coefs_and_monomials(p::Polynomial, ::Type{T}) where {T <: Integer}
  local co = coeffs(p)
  local mo = nonzero_monomials(co, T)
  (co[map(adder(firstindex(co)), mo)], mo)
end

CompressedPolynomial{F, var_x, var_y, Polynomial{F, var_y}}(
  p::Polynomial{F, var_x}) where {F <: Real, var_x, var_y} =
  CompressedPolynomial{F, var_x, var_y, Polynomial{F, var_y}}(
    polynomial_to_coefs_and_monomials(p, UInt8)...)

end
