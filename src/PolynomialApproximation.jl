# Copyright © 2022 Neven Sajko. All rights reserved.

module PolynomialApproximation

include("DFloats.jl")
include("ScaledDFloats.jl")
include("NumericalError.jl")
include("IntervalHelpers.jl")
include("Perturbed.jl")

include("OverRing.jl")
include("CompressedPolynomials.jl")
include("RFactoredPolynomials.jl")
include("TranslatedPolynomials.jl")

include("MinimalPolynomialPassingThroughIntervals.jl")

include("MFloats.jl")
include("AbstractValues.jl")
include("ValueTypes.jl")
include("Values.jl")
include("Names.jl")
include("NameMFloats.jl")
include("FloatingPointConstants.jl")
include("FPConstMFloats.jl")
include("TypedValues.jl")
include("Operations.jl")
include("Assignments.jl")
include("CodeGen.jl")

include("ProgramPrinting.jl")

using
  AMRVW,
  Combinatorics,
  Random,
  Polynomials,
  PolynomialPassingThroughIntervals.NumericalErrorTypes,
  .DFloats,
  .ScaledDFloats,
  .NumericalError,
  .OverRing,
  .CompressedPolynomials,
  .RFactoredPolynomials,
  .TranslatedPolynomials

export prepare_polynomial

function vector_divider_iterator(
  v::AbstractVector,
  element_length::Int,
  iterator_length::Int)

  ==(element_length * iterator_length, length(v)) || error("size mismatch")

  ((@view v[(begin + i*element_length):(begin + (i + 1)*element_length - 1)])
    for i in 0:(iterator_length - 1))
end

const rand_diff_dist = map(Int8, (-31):31)

function rand_diffs!(
  v::AbstractVector,
  element_length::Int,
  iterator_length::Int)

  ==(element_length * iterator_length, length(v)) || error("size mismatch")

  rand!(v, rand_diff_dist)
  vector_divider_iterator(v, element_length, iterator_length)
end

with_delta_applied(p::AbstractFloat, d::Integer) =
  nextfloat(p, d)

with_delta_applied(p::NTuple{n, <:AbstractFloat}, d::NTuple{n, <:Integer}) where {n} =
  map(with_delta_applied, p, d)

function with_delta_applied(
  p::AbstractVector{<:AbstractFloat},
  d::AbstractVector{<:Integer})

  ==(map(length, (p, d))...) || error("size mismatch")

  map(with_delta_applied, p, d)
end

with_delta_applied(p::P, d::AbstractVector{<:Integer}) where {P <: Polynomial} =
  P(with_delta_applied(coeffs(p), d))

function monic_immutable_with_delta(p::P, d::NTuple{n, <:Integer}) where
{v, m, F, P <: ImmutablePolynomial{F, v, m}, n}
  ==(m, n + 1) || error("wrong number of coefficients")
  local c = coeffs(p)
  isone(last(c)) || error("polynomial not monic")
  P((map(with_delta_applied, Base.front(c), d)..., one(F)))
end

tuple_slice_of_vector(v::AbstractVector, start::Int, ::Val{len}) where {len} =
  ntuple(
    let v = v, s = start
      i -> v[begin + start + i - 1]
    end,
    Val{len}())

function monic_immutables_with_delta(
  p::AbstractVector{P},
  d::AbstractVector{<:Integer}) where {v, m, P <: ImmutablePolynomial{<:Any, v, m}}

  local len = m - 1

  ==(length(p) * len, length(d)) || error("size mismatch")

  local ret = P[]

  for i in 0:(length(p) - 1)
    push!(
      ret,
      monic_immutable_with_delta(
        p[begin + i],
        tuple_slice_of_vector(d, i * len, Val{len}())))
  end

  ret
end

with_delta_applied(p::P, d::AbstractVector{<:Integer}) where
{v, F, P <: RFactoredPolynomial{F, v}} =
  P(
    with_delta_applied(p.constant_factor, first(d)),
    monic_immutables_with_delta(
      p.linear_factors,
      @view d[(begin + 1):(begin + length(p.linear_factors))]),
    monic_immutables_with_delta(
      p.quadratic_factors,
      @view d[(begin + length(p.linear_factors) + 1):end]))

with_delta_applied(p::P, d::AbstractVector{<:Integer}) where
{P <: TranslatedPolynomial} =
  P(
    with_delta_applied(p.offset, first(d)),
    with_delta_applied(p.p, @view d[(begin + 1):end]))

with_delta_applied(p::P, d::AbstractVector{<:Integer}) where
{P <: CompressedPolynomial} =
  P(p.l, p.k, with_delta_applied(p.main_factor, d))

# Use fma to improve accuracy in this case.
function Base.evalpoly(
  x::Real,
  p::TranslatedPolynomial{F, v, RFactoredPolynomial{F, v}}) where {F <: Real, v}

  local factors = RFactoredPolynomials.evaluated_factors(x, p.p)
  fma(RFactoredPolynomials.reduce_binary_tree_mul(factors)..., p.offset)
end

max_error(
  p::AbstractPolynomial,
  inputs::AbstractVector{Small},
  accurate_outputs::AbstractVector{Big},
  error_type::NumericalErrorType) where {Small <: AbstractFloat, Big <: Any} =
  mapreduce(
    let p = p
      (i, o) -> safe_error_func(error_type)(o, evalpoly(i, p))
    end,
    (l, r) -> (<(l, r) ? r : l),
    inputs,
    accurate_outputs,
    init = safe_error_func(error_type)(one(Big), one(Small)))

struct PolynomialWithErrorAndDiff{P <: AbstractPolynomial, F <: Union{<:Real, <:DFloat}}
  p::P
  e::F
  d::Vector{Int8}
end

const AccurateType = Union{<:Real, <:DFloat, <:ScaledDFloat}

function best_neighbor(
  pol::P,
  inputs::AbstractVector{Small},
  accurate_outputs::AbstractVector{Big},
  error_type::NumericalErrorType,
  iter_cnt::Int) where
{Small <: AbstractFloat, Big <: AccurateType, P <: AbstractPolynomial{Small}}

  ==(map(length, (inputs, accurate_outputs))...) || error("size mismatch")

  local best = P(pol)
  local best_max_err = max_error(pol, inputs, accurate_outputs, error_type)

  local len = length(pol)

  local best_diff = zeros(Int8, len)

  local diffs_vec = zeros(Int8, len * iter_cnt)

  for d in rand_diffs!(diffs_vec, len, iter_cnt)
    local p = with_delta_applied(pol, d)
    local max_err = max_error(p, inputs, accurate_outputs, error_type)

    if max_err < best_max_err
      best_diff .= d
      best_max_err = max_err
      best = p
    end
  end

  PolynomialWithErrorAndDiff(best, best_max_err, best_diff)
end

with_translated_main_part(o::F, p::Polynomial{F}) where {F <: Real} =
  offset_polynomial(o, p)

with_translated_main_part(o::F, p::TranslatedPolynomial{F}) where {F <: Real} =
  TranslatedPolynomial{F}(p.offset, with_translated_main_part(o, p.p))

with_translated_main_part(o::F, p::CompressedPolynomial{F, x}) where {F <: Real, x} =
  CompressedPolynomial{F, x}(p.l, p.k, with_translated_main_part(o, p.main_factor))

with_factored_main_part(p::Polynomial{F, v}) where {F <: Real, v} =
  fromroots(RFactoredPolynomial{F, v}, AMRVW.roots(coeffs(p))) * last(coeffs(p))

with_factored_main_part(p::TranslatedPolynomial) =
  TranslatedPolynomial(p.offset, with_factored_main_part(p.p))

with_factored_main_part(p::CompressedPolynomial{F, x}) where {F <: Real, x} =
  CompressedPolynomial{F, x}(p.l, p.k, with_factored_main_part(p.main_factor))

with_permuted_factors(p::P, lin::Vector{<:Integer}, quad::Vector{<:Integer}) where
{P <: TranslatedPolynomial} =
  P(p.offset, with_permuted_factors(p.p, lin, quad))

with_permuted_factors(p::P, lin::Vector{<:Integer}, quad::Vector{<:Integer}) where
{P <: CompressedPolynomial} =
  P(p.l, p.k, with_permuted_factors(p.main_factor, lin, quad))

with_permuted_factors(p::P, lin::Vector{<:Integer}, quad::Vector{<:Integer}) where
{P <: RFactoredPolynomial} =
  P(p.constant_factor, p.linear_factors[lin], p.quadratic_factors[quad])

factor_permutations_iterator(p::TranslatedPolynomial) =
  factor_permutations_iterator(p.p)

factor_permutations_iterator(p::CompressedPolynomial) =
  factor_permutations_iterator(p.main_factor)

factor_permutations_iterator(p::RFactoredPolynomial) =
  Iterators.product(
    permutations(axes(p.linear_factors, 1)),
    permutations(axes(p.quadratic_factors, 1)))

abstract type AbstractPreparation end

struct HornerPreparation <: AbstractPreparation end

struct FactoredPreparation <: AbstractPreparation end

struct ComplicatedPreparation{F <: Real} <: AbstractPreparation
  offset::F
end

compressed_factored(p::Polynomial{F, var_x}, ::Val{var_y}) where
{F <: Real, var_x, var_y} =
  with_factored_main_part(CompressedPolynomial{F, var_x, var_y, Polynomial{F, var_y}}(p))

compressed_translated_factored(p::Polynomial{F, var_x}, offset::F, ::Val{var_y}) where
{F <: Real, var_x, var_y} =
  with_factored_main_part(with_translated_main_part(
    offset,
    CompressedPolynomial{F, var_x, var_y, Polynomial{F, var_y}}(p)))

function best_neighbor_complicated(
  pol::Polynomial{Big, var_x},
  offset::Big,
  ::Val{var_y},
  inputs::AbstractVector{Small},
  accurate_outputs::AbstractVector{<:AccurateType},
  error_type::NumericalErrorType,
  iter_cnt::Int) where {Big <: Real, Small <: Real, var_x, var_y}

  local p = over_ring(Small, compressed_translated_factored(pol, offset, Val{var_y}()))

  local ret = best_neighbor(p, inputs, accurate_outputs, error_type, iter_cnt)

  # TODO: going through all permutations is redundant and wasteful

  for (per_lin, per_quad) in factor_permutations_iterator(p)
    local q = with_permuted_factors(p, per_lin, per_quad)
    local ib = best_neighbor(q, inputs, accurate_outputs, error_type, iter_cnt)
    <(ib.e, ret.e) && (ret = ib)
  end

  ret
end

prepare_polynomial(
  p::Polynomial{Big, var_x},
  inputs::AbstractVector{Small},
  accurate_outputs::AbstractVector{<:AccurateType},
  iter_cnt::Int,
  ::Val{var_y},
  option::ComplicatedPreparation{<:Big},
  error_type::NumericalErrorType = RelativeError()) where
{var_x, var_y, Small <: AbstractFloat, Big <: Real} =
  best_neighbor_complicated(
    p,
    option.offset,
    Val{var_y}(),
    inputs,
    accurate_outputs,
    error_type,
    iter_cnt)

prepare_polynomial(
  p::Polynomial{Big, var_x},
  inputs::AbstractVector{Small},
  accurate_outputs::AbstractVector{<:AccurateType},
  iter_cnt::Int,
  ::Val{var_y},
  ::FactoredPreparation,
  error_type::NumericalErrorType = RelativeError()) where
{var_x, var_y, Small <: AbstractFloat, Big <: Real} =
  best_neighbor(
    over_ring(Small, compressed_factored(p, Val{var_y}())),
    inputs,
    accurate_outputs,
    error_type,
    iter_cnt)

prepare_polynomial(
  p::Polynomial{Big, var_x},
  inputs::AbstractVector{Small},
  accurate_outputs::AbstractVector{<:AccurateType},
  iter_cnt::Int,
  ::Val{var_y},
  ::HornerPreparation = HornerPreparation(),
  error_type::NumericalErrorType = RelativeError()) where
{var_x, var_y, Small <: AbstractFloat, Big <: Real} =
  best_neighbor(
    CompressedPolynomial{
      Small,
      var_x,
      var_y,
      Polynomial{Small, var_y}}(over_ring(Small, p)),
    inputs,
    accurate_outputs,
    error_type,
    iter_cnt)

end
