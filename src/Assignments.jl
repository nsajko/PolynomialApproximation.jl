# Copyright © 2023 Neven Sajko. All rights reserved.

module Assignments

using
  ..AbstractValues, ..ValueTypes, ..TypedValues,
  ..Names, ..FloatingPointConstants,
  ..Operations

export Assignment, Program, check_ssa_form, unparse_program

# TODO: add a comment field if necessary
struct Assignment{name_sep}
  name::Name{name_sep}
  op::Operation
  args::Vector{TypedValue{name_sep}}

  function Assignment{ns}(
    n::Name{ns},
    o::Operation,
    a::Vector{TypedValue{ns}}) where {ns}

    local arg_count = length(a)
    local ari = Operations.arity(o)
    ==(arg_count, ari) ||
      error("expected arity $ari, but $arg_count arguments found")

    local operands_expected = Operations.operand_types(o)
    local operands_got = map(value_type, a)
    ==(operands_expected, operands_got) ||
      error("expected operands $operands_expected, got $operands_got")

    new{ns}(n, o, a)
  end
end

Assignment(n::Name{sep}, o::Operation, a::Vector{Name{sep}}) where {sep} =
  Assignment{sep}(n, o, map(TypedValue{sep}, a))

Assignment(n::Name{sep}, a::AbstractFloat) where {sep} =
  Assignment{sep}(n, Operations.fpc, [TypedValue{sep}(a)])

const Program = AbstractVector{Assignment{ns}} where {ns}

var_is_used_in_assignment(name::Name) =
  let n = name
    function(ass::Assignment)
      for arg in ass.args
        (==(value_type(arg), ValueTypes.name) & ==(value(arg), value(n))) &&
          (return true)
      end
      false
    end
  end

var_is_unused_in_prog(name::Name, prog::Program) =
  !any(var_is_used_in_assignment(name), prog)

function unused_vars(as::Program)
  local ret = Name[]
  for i in 0:(length(as) - 2)
    local n = as[begin + i].name
    var_is_unused_in_prog(n, @view as[(begin + i + 1):end]) && push!(ret, n)
  end
  ret
end

function vars_used_before_definition(as::Program)
  local ret = Name[]
  for i in 0:(length(as) - 1)
    local n = as[begin + i].name
    var_is_unused_in_prog(n, @view as[begin:(begin + i)]) || push!(ret, n)
  end
  ret
end

is_in_ssa_form(as::Program) =
  allunique(map(a -> a.name, as))

function check_ssa_form(as::Program)
  is_in_ssa_form(as) || error("duplicate names detected")
  local uv = unused_vars(as)
  local vubd = vars_used_before_definition(as)
  (isempty(uv) & isempty(vubd)) ||
    error("unused vars: ", uv, "\nvars used before definition: ", vubd)
  nothing
end

"""
Converts a vector of assignments into a block of variable declarations.
"""
function unparse_program(to_var_decl::F, program::Program) where {F <: Function}
  check_ssa_form(program)
  mapreduce(to_var_decl, *, program, init = "")
end

end
