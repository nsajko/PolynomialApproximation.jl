# Copyright © 2023 Neven Sajko. All rights reserved.

module ScaledDFloats

using ..DFloats

export ScaledDFloat

# Represents ldexp(f, e), i.e., f * 2^e.
struct ScaledDFloat{T <: AbstractFloat}
  # Between approximately 1 and 2
  f::DFloat{T}

  e::Int32

  function ScaledDFloat{T}(x::AbstractFloat) where {T <: AbstractFloat}
    iszero(x) && (return new{T}(zero(DFloat{T}), zero(Int32)))

    local e = Int32(exponent(x))
    local y = ldexp(x, -e)
    new{T}(DFloat{T}(T(y)), e)
  end
end

Base.BigFloat(f::ScaledDFloat) = ldexp(BigFloat(f.f), f.e)

Base.isfinite(f::ScaledDFloat) = isfinite(f.f)

Base.iszero(f::ScaledDFloat) = iszero(f.f)

Base.isone(f::ScaledDFloat) = iszero(f.e) & isone(f.f)

Base.zero(::Type{ScaledDFloat{T}}) where {T <: AbstractFloat} =
  ScaledDFloat{T}(0.0f0)

Base.one(::Type{ScaledDFloat{T}}) where {T <: AbstractFloat} =
  ScaledDFloat{T}(1.0f0)

end
