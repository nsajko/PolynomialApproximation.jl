# Copyright © 2023 Neven Sajko. All rights reserved.

module Values

using ..AbstractValues, ..ValueTypes

export Value

struct Value <: AbstractValue
  s::String

  function Value(v::String)
    isempty(v) &&
      error("an empty Value is not allowed")
    any(isspace, v) &&
      error("a Value is not allowed to contain whitespace")
    any(!isprint, v) &&
      error("a Value must contain only printable characters")
    new(v)
  end
end

AbstractValues.value(v::Value) = v

Base.string(v::Value) = v.s

end
