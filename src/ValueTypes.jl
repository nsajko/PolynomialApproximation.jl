# Copyright © 2023 Neven Sajko. All rights reserved.

module ValueTypes

using ..AbstractValues

export
  ValueType, value_type

# TODO:
#
# The implementation is kind of a hack, what I really want are enums.
# A quick JuliaHub packages search turned up two very similar packages:
# EnumX and SuperEnums. I could use one of those, but it bothers me
# that both of those use macros and this doesn't seem necessary. So
# I'll try to write my own package that constructs a module with
# `Module` and `eval`s the enum code into it, without any macros.

struct ValueType
  t::Int8

  function ValueType(t::Integer)
    (0 <= t < 2) || error("unsupported value type")
    new(t)
  end
end

"""
The name of a variable
"""
const name = ValueType(0)

"""
A floating-point constant
"""
const fpc = ValueType(1)

value_type(::AbstractValue) = error("not supported or currently not implemented")

end
