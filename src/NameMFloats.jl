# Copyright © 2023 Neven Sajko. All rights reserved.

module NameMFloats

using ..Names

export NameMFloat

struct NameMFloat{name_sep}
  f::Vector{Name{name_sep}}

  function NameMFloat{ns}(f::Vector{Name{ns}}) where {ns}
    isempty(f) && error("this vector must not be empty")
    new{ns}(f)
  end
end

NameMFloat(f::Vector{Name{ns}}) where {ns} = NameMFloat{ns}(f)

end
