# Copyright © 2023 Neven Sajko. All rights reserved.

module ProgramPrinting

using
  Polynomials,
  ..Operations,
  ..Names,
  ..TypedValues,
  ..NameMFloats,
  ..Assignments,
  ..CodeGen

export
  Settings, Language,
  polynomial_to_source_code_function

struct Op{op}
  function Op{op}() where {op}
    isa(op, Operation) || error("op should be an Operation")
    new{op}()
  end
end

Base.print(io::IO, ::Op{op}) where {op} = print(io, op)

struct Language{lang}
  function Language{lang}() where {lang}
    isa(lang, Symbol) || error("try a Symbol, like :julia, :cxx, :go, :rust or :java")
    new{lang}()
  end
end

struct Settings{lang}
  fp_type_name::String
  indentation::String

  function Settings{lang}(fp_type_name::String, indent::String) where {lang}
    isa(lang, Language) || error("lang should be a Language")
    new{lang}(fp_type_name, indent)
  end
end

const lang_julia = Language{:julia}()
const lang_cxx = Language{:cxx}()
const lang_go = Language{:go}()
const lang_rust = Language{:rust}()
const lang_java = Language{:java}()

var_decl_prefix(::Assignment, set::Settings{lang_julia}) =
  "$(set.indentation)local "

var_decl_prefix(::Assignment, set::S) where
{S <: Union{Settings{lang_cxx}, Settings{lang_java}}} =
  "$(set.indentation)$(set.fp_type_name) "

var_decl_prefix(::Assignment, set::Settings{lang_go}) =
  "$(set.indentation)var "

var_decl_prefix(::Assignment, set::Settings{lang_rust}) =
  "$(set.indentation)let "

var_decl_infix(::Assignment, set::Settings{lang_julia}) =
  "::$(set.fp_type_name) = "

var_decl_infix(::Assignment, ::S) where
{S <: Union{Settings{lang_cxx}, Settings{lang_java}}} =
  " = "

var_decl_infix(::Assignment, set::Settings{lang_go}) =
  " $(set.fp_type_name) = "

var_decl_infix(::Assignment, set::Settings{lang_rust}) =
  ": $(set.fp_type_name) = "

var_decl_suffix(::Assignment, ::S) where
{S <: Union{Settings{lang_julia}, Settings{lang_go}}} =
  "\n"

var_decl_suffix(::Assignment, ::S) where
{S <: Union{Settings{lang_cxx}, Settings{lang_rust}, Settings{lang_java}}} =
  ";\n"

printed_operation(::Op{Operations.fpc}, arg::TypedValue, ::Settings) =
  "$arg"

add_name(::Settings) = "+"
mul_name(::Settings) = "*"
fma_name(::Settings{lang_julia}) = "fma"
fma_name(::Settings{lang_cxx}) = "std::fma"
fma_name(::Settings{lang_go}) = "math.FMA"
fma_name(::Settings{lang_rust}) = "mul_add"
fma_name(::Settings{lang_java}) = "Math.fma"

operation_name(::Op{Operations.add}, set::Settings) = add_name(set)
operation_name(::Op{Operations.mul}, set::Settings) = mul_name(set)
operation_name(::Op{Operations.fma}, set::Settings) = fma_name(set)

function printed_operation(op::O, arg::NTuple{2, <:TypedValue}, set::Settings) where
{O <: Union{Op{Operations.add}, Op{Operations.mul}}}
  local (l, r) = arg
  local o = operation_name(op, set)
  "$l $o $r"
end

function printed_operation(
  ::Op{Operations.fma}, arg::NTuple{3, <:TypedValue}, set::Settings)

  local (a, b, c) = arg
  local fma = fma_name(set)
  "$(fma)($a, $b, $c)"
end

function printed_operation(
  ::Op{Operations.fma}, arg::NTuple{3, <:TypedValue}, set::Settings{lang_rust})

  local (a, b, c) = arg
  local fma = fma_name(set)
  "$(a).$(fma)($b, $c)"
end

tuple_from_vector(vector::AbstractVector, ::Val{n}) where {n} = ntuple(
  let v = vector
    i -> vector[begin + i - 1]
  end,
  Val{n}())

printed_operation(op::Operation, args::AbstractVector{<:TypedValue}, set::Settings) =
  if op == Operations.fpc
    printed_operation(Op{op}(), only(args), set)
  elseif op ∈ (Operations.add, Operations.mul)
    printed_operation(Op{op}(), tuple_from_vector(args, Val{2}()), set)
  elseif op == Operations.fma
    printed_operation(Op{op}(), tuple_from_vector(args, Val{3}()), set)
  else
    error("unexpected: operation not implemented")
  end

assignment_to_var_decl(a::Assignment, set::Settings) = string(
  var_decl_prefix(a, set),
  a.name,
  var_decl_infix(a, set),
  printed_operation(a.op, a.args, set),
  var_decl_suffix(a, set))

fun_decl_prefix(name::String, arg_name::Name, set::Settings{lang_julia}) =
  "function $(name)($arg_name::$(set.fp_type_name))\n"

function fun_decl_prefix(name::String, arg_name::Name, set::Settings{lang_cxx})
  local type = set.fp_type_name
  "$type $(name)($type $arg_name) {\n"
end

function fun_decl_prefix(name::String, arg_name::Name, set::Settings{lang_go})
  local type = set.fp_type_name
  "func $(name)($arg_name $type) $type {\n"
end

function fun_decl_prefix(name::String, arg_name::Name, set::Settings{lang_rust})
  local type = set.fp_type_name
  "fn $(name)($arg_name: $type) -> $type {\n"
end

function fun_decl_prefix(name::String, arg_name::Name, set::Settings{lang_java})
  local type = set.fp_type_name
  "static $type $(name)($type $arg_name) {\n"
end

fun_decl_suffix(::String, ::Name, set::Settings{lang_julia}, result::NameMFloat) =
  "$(set.indentation)$(first(result.f))\n" *
  "end\n"

fun_decl_suffix(::String, ::Name, set::S, result::NameMFloat) where
{S <: Union{Settings{lang_cxx}, Settings{lang_java}}} =
  "$(set.indentation)return $(first(result.f));\n" *
  "}\n"

fun_decl_suffix(::String, ::Name, set::Settings{lang_go}, result::NameMFloat) =
  "$(set.indentation)return $(first(result.f))\n" *
  "}\n"

fun_decl_suffix(::String, ::Name, set::Settings{lang_rust}, result::NameMFloat) =
  "$(set.indentation)$(first(result.f))\n" *
  "}\n"

function polynomial_to_source_code_function(
  p::AbstractPolynomial,
  name::String,
  set::Settings,
  arg_name::Name{ns} = Name("x")) where {ns}

  local (prog, result) = implement_polynomial_as_program(p, arg_name, Name{ns}("root"))

  string(
    fun_decl_prefix(name, arg_name, set),
    unparse_program((a::Assignment) -> assignment_to_var_decl(a, set), prog),
    fun_decl_suffix(name, arg_name, set, result))
end

end
