# Copyright © 2023 Neven Sajko. All rights reserved.

module AbstractValues

export AbstractValue, value

abstract type AbstractValue end

value(::AbstractValue) = error("not supported or currently not implemented")

Base.print(io::IO, v::AbstractValue) = print(io, string(value(v)))

end
