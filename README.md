# PolynomialApproximation

## Usage example

Import some packages:
```julia
using Polynomials, PolynomialPassingThroughIntervals, PolynomialApproximation
```

Increase `BigFloat` precision:
```julia
setprecision(BigFloat, 2^11)
```

Define a domain for the polynomial we're seeking:
```julia
poldom = Rational{BigInt}[x//1024 for x in big"0":big"127"];
```

Suppose we're trying to approximate the cosine on that small domain:
```julia
cos_values = map(cos ∘ BigFloat, poldom);
```

Specify our maximum allowed error (it's relative error in this example):
```julia
max_error = eps(Float64) * 2.0^20
```

Specify allowed intervals of the the polynomial we seek, given the maximum error.
Note that `intervals_given_max_rel_err` is just a helper function, the intervals may
be arbitrarily specified (this makes this method more powerful than Remez-based
software, like Sollya, as far as I understand):
```julia
cos_intervals = intervals_given_max_rel_err(cos_values, max_error);
```

Specify allowed monomials for the search. This restricts the search to even
polynomials, which might make sense because the cosine is an even function. Specify
`0:16` instead to just search for any small polynomial:
```julia
monomials = UInt8[i for i in 0:2:16]
```

Commence the search (linear optimization). The output will be ugly because of the
horrendous number of digits, feel free to do `map(Float64, coefs)` to make it more
presentable, though:
```julia
coefs, monomials = minimal_polynomial_passing_through_intervals(
  monomials, poldom, cos_intervals,
  PolynomialPassingThroughIntervals.ReplacedVariablesOption{:determinants}())
```

Present the polynomial with coefficients rounded to a friendlier number of digits:
```julia
Polynomial{Float64}(
  reconstruct_coefficients_from_monomial_indices(coefs, monomials))
```

Convert to a `Polynomial`:
```julia
simple_poly = Polynomial(
  reconstruct_coefficients_from_monomial_indices(coefs, monomials));
```

Now we want to prepare the polynomial we found for implementing it in Float64
arithmetic:
```julia
rounded_dom = map(Float64, poldom);
```

We'll need to specify the type of error we're interested in again. Zeros currently
present a problem for relative error. Maybe I try and add some kind of support for
ignoring removable discontinuities of relative error in the future.
```julia
function safe_relative_error(accur::Number, approx::Number)
  iszero(accur) && error("division by zero")
  absolute_error(accur, approx) / abs(accur)
end
```

Each of the following two function calls will "compress" the polynomial before doing
almost anything else. Compressing is a term I made up for getting rid of zero roots
and performing a simple change of variables so as to decrease the degree of the
polynomial.

Prepare the polynomial for evaluation with Horner's scheme:
```julia
prepared_horner = prepare_polynomial(
  simple_poly, rounded_dom, cos_values, safe_relative_error, 2^17, Val{:y}())
```

Prepare the polynomial for evaluation with a certain kind of somewhat factorized
scheme instead. The array literal specifies offsets for vertical translation of the
polynomial before factorization:
```julia
prepared_complicated = prepare_polynomial(
  simple_poly,
  rounded_dom,
  cos_values,
  safe_relative_error,
  2^17,
  Val{:y}(),
  PolynomialApproximation.ComplicatedPreparation(BigFloat[i/8 for i in -12:12]))
```

The previous two commands can take a long time, depending on the argument `2^17`,
because they search for the best way to round the coefficients. The current naive
method of accomplishing this, however, isn't very effective. Other software, like
Sollya, uses lattice reduction for finding a good representation in a smaller type
for the vector of coefficients. That would probably be more effective.

Furthermore, `prepared_complicated` takes time proportional with the length of its
last argument, which determines the offset of the vertical translation that is
performed prior to factorization, so the search and factorization are repeated for
each offset.

Let's see how good our results are:

```none
julia> max_error
2.3283064365386963e-10

julia> prepared_horner.best.e
3.3842709532244195e-10
```

So Horner's scheme results in an error slightly larger than requested. This
shouldn't be a problem though, if a smaller error in the results is necessary, just
specify a smaller error initially, it should help.

The complicated scheme sadly results in unacceptably large errors for all tested
offsets:
```
julia> map(ib -> ib.best.e, prepared_complicated)
25-element Vector{Float64}:
  0.1781618100108304
  2.8771013250660715
  3.077533364808644
  5.754202652596537
  5.976904919606457
  8.631303980127006
  8.876276474404271
 11.508405307657469
 11.775648029202085
 14.385506635187937
 17.262607962718405
 20.139709290248867
 23.016810628371676
 25.91618218316949
 28.815553737967303
 31.714925292765116
 34.61429684756293
 37.51366840236076
 40.41303995715857
 43.312411511956384
 46.2117830667542
 49.11115462155201
 52.01052617634983
 54.90989773114764
 57.809269285945454
```

The errors vary wildly between different offsets, though. So maybe a better result
could be obtained if we could find a good value for the offset.
